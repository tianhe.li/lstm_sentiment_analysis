import os
import math
import pandas as pd
import numpy as np
import torch
from torch.utils.data import TensorDataset, DataLoader
import torch.nn as nn
import string
import json
import matplotlib.pyplot as plt
import mplcyberpunk
import PySimpleGUI as sg
import pickle
from pdf_maker import Graphs
from reportlab.platypus import SimpleDocTemplate
from reportlab.lib.pagesizes import A4
from reportlab.lib.colors import HexColor
from reportlab.pdfgen.canvas import Canvas

def myLaterPages(canvas, doc):
    canvas.saveState()
    canvas.setFillColor(HexColor("#212946"))
    canvas.rect(0,0,9999,9999,fill=1)
    canvas.restoreState()

def Review_Error(canvas, doc):
    canvas.saveState()
    canvas.setFillColor(HexColor("#FF0000"))
    canvas.rect(0,0,9999,9999,fill=1)
    canvas.restoreState()

# load the document for vocab and change it into a dic
def load_vocab(data_dir):
    current_work_dir = os.path.dirname (__file__)
    vocab = open(os.path.join(current_work_dir, data_dir)).read().split('\n')
    vocab = ['_PAD'] + vocab     # use the word '_PAD' to fill short reviews, if needed
    vocab = {o:i for i,o in enumerate(vocab)}
    return vocab

# controlling the length of the sentences, in order to reduce the error 
def pad_fill(sentences, length):
    features = np.zeros((len(sentences), length),dtype=int)
    for j, reviews in enumerate(sentences):
        if len(reviews) != 0:
            features[j, -len(reviews):] = np.array(reviews)[:length]
    return features

# a class for deep learning, I wrote it for training the AI, only some of the functions and variables are been used in this python script
class SentimentNet(nn.Module):
    def __init__(self, vocab_size):
        super(SentimentNet, self).__init__()
        self.n_layers = n_layers = 2  # layers for LSTM
        self.hidden_dim = hidden_dim = 256  # hidden stae = 256
        embedding_dim = 512  # change words to a 512 dimention vector
        drop_prob = 0.5  # dropout

        self.embedding = nn.Embedding(vocab_size, embedding_dim)

        self.lstm = nn.LSTM(embedding_dim,  # input the dimention
                            hidden_dim,  # hidden state
                            n_layers,  # LSTM layers
                            dropout=drop_prob,
                            batch_first=True  # check if the first layer is 'batch_sized'
                            )
        self.layer_norm = nn.LayerNorm(hidden_dim)  # Add LayerNorm here
        self.fc = nn.Linear(in_features=hidden_dim, out_features=1)
        self.sigmoid = nn.Sigmoid()
        self.dropout = nn.Dropout(drop_prob)  # add a dropout at last

    def forward(self, x, hidden):
        batch_size = x.size(0)
        x = x.long()
        embeds = self.embedding(x)
        lstm_out, hidden = self.lstm(embeds, hidden)
        lstm_out = lstm_out.contiguous().view(-1, self.hidden_dim)

        # out = self.layer_norm(lstm_out)  # Apply LayerNorm here
        out = self.dropout(lstm_out)
        out = self.fc(out)
        # out = self.sigmoid(out)
        out = out.view(batch_size, -1)
        out = out[:, -1]
        return out, hidden

    def init_hidden(self, btach_size):
        hidden = (torch.zeros(self.n_layers, batch_size, self.hidden_dim).to(device),
                  torch.zeros(self.n_layers, batch_size, self.hidden_dim).to(device)
                  )
        return hidden
    # I did not fully understand the hidden layers, but with the help of my friends, I finished the code and it worked, kind of.

def sentence_encoder(sentence):
    sentence = ''.join(['' if word in string.punctuation.replace("'",'') else word for word in sentence])
    return sentence


if torch.cuda.is_available():   # choosing the device in between cuda, apple silicon, or cpu
    device = torch.device('cuda')
elif torch.backends.mps.is_available():   # I'm not sure if this is the correct code, cuz it runs extremely sow on Hector's macbook :(
    device = torch.device('mps')          # It feels like it's not using apple's gpu. Since I do not have a macbook, I'll fix it later
else:
    device = torch.device('cpu')

vocab = load_vocab(r'Data/aclImdb/imdb.vocab')   # load the default model, which is model 4
model_default = SentimentNet(len(vocab))
model_default.load_state_dict(torch.load(r'./models/model_4.pth', map_location=device))
model_default.to(device)

def predict(path, inputs):    # do the predict while allowing the user to choose the model
    model = SentimentNet(len(vocab))
    model.load_state_dict(torch.load(path, map_location=device))  # load the model with the correct device, there are three models included in the folder, which includes a standard model, an overfitted model, and an underfitted model. Three models behaves slightly differently.
    model.to(device)
    inputs = sentence_encoder(inputs)
    sentences=[[vocab[word.lower()] if word.lower() in vocab else 0 for word in inputs.split(' ')]]  # encode the sentence
    sentences = pad_fill(sentences, 200)
    sentences = torch.Tensor(sentences).long().to(device)
    h = (torch.Tensor(2,1,256).zero_().to(device),
         torch.Tensor(2,1,256).zero_().to(device))
    h = tuple([each.data for each in h])
    return (float(model(sentences,h)[0])+2)/4  # decode the result

def quick_predict(inputs):  # do the predict using the default model
    inputs = sentence_encoder(inputs)
    sentences=[[vocab[word.lower()] if word.lower() in vocab else 0 for word in inputs.split(' ')]]  # encode the sentence
    sentences = pad_fill(sentences, 200)
    sentences = torch.Tensor(sentences).long().to(device)
    h = (torch.Tensor(2,1,256).zero_().to(device),
         torch.Tensor(2,1,256).zero_().to(device))
    h = tuple([each.data for each in h])
    return (float(model_default(sentences,h)[0])+2)/4  # decode the result

def to_binary(inputs):  # output either positive or negative
    if inputs >= 1:  # the result might go beyond 1 when the review is extremely positive
        inputs = 1
    elif inputs <= 0:  # the result might go below 0 when the review is extremely negative
        inputs = 0
    else:
        inputs = round(inputs, 0)  # round the number
    return inputs

def load_json_file(data_dir):  # load the file and change it into a df that only have one line, which is a one denmitional 
    df = pd.read_json(data_dir)
    del df['title']
    df = df.melt()
    return df.value

def df_predict(path, df):   # predict all the information in a dataframe
    result_list = []
    counter = 0
    for i in df:
        counter += 1
        sg.one_line_progress_meter(
        'loading', counter, len(df),
        'Predicting Data'
        )
        result_list.append(predict(path, i))
    return result_list  # return a list that contain the results

def df_quick_predict(df):   # predict all the information in a dataframe using default model
    result_list = []
    counter = 0
    for i in df:
        counter+=1
        sg.one_line_progress_meter(
        'loading', counter, len(df),
        'Predicting Data'
        )
        result_list.append(quick_predict(i))
    return result_list   # return a list that contain the results
    
plt.style.use('cyberpunk')  # using the module mplcyberpunk to create some special effect on our graphs

def bar_review_scores_increasing(ls, title, xl, yl):  # create a bar graph using scores as the x axis
    ls = pd.DataFrame(ls, columns = ['value']).round(1)
    ls.value = [1 if i>1 else i for i in ls.value]
    ls.value = [0 if i<0 else i for i in ls.value]
    ls.sort_values(by=['value'], inplace=True)
    ls = ls.value.value_counts().sort_index().to_frame()
    plt.title(title)
    plt.xlabel(xl)
    plt.ylabel(yl)
    bars = plt.bar((ls.index*10).tolist(), ls.value.tolist())
    mplcyberpunk.add_bar_gradient(bars=bars)

def plot_review_time_increasing(ls, title, xl, yl):  # plot a graph using time as the x axis
    ls = pd.DataFrame(ls, columns = ['value'])
    ls.value = [1 if i>1 else i for i in ls.value]
    ls.value = [0 if i<0 else i for i in ls.value]
    plt.title(title)
    plt.xlabel(xl)
    plt.ylabel(yl)
    try:
        ls = ls.sample(n=15*(len(ls.value)//15))  # in case we don't have enough data to plot this graph
        plt.plot(range(int(len(ls.value)/15)), list(np.mean(np.array_split(np.array(ls),15), axis=0)*10))
        mplcyberpunk.add_glow_effects(gradient_fill=True)
    except:
        print('not enough data for box office estimation')  # print the situation and do not create the graph

def box_office_preprocess(df, avg):  # preprocess the data from box office estimation
    if df.iloc[0,0] == 0 or len(avg) == 0:  # checking if we have enough data for estimation
        print('movie not found')
        arr=np.zeros((1,5))
        return pd.DataFrame(arr, columns=['Director', 'Genre', 'Country', 'Rated', 'imdb_score'])
    else:
        avg = pd.DataFrame([sum(avg)*10/len(avg)], columns=['imdb_score'])  # calculating the avg score
        df_new = pd.concat([df,avg], axis=1)  # concat two dataframes
        return df_new

def box_office_predict(x):  # predict the box office using the machine learning model
    if x.iloc[0,0] == 0:  # checking if we have enough data for estimation
        print('not enough data for box office estimation')
        return 0
    else:  # estimate the box office
        dn = load_vocab(r'director_names.txt')
        ct = load_vocab(r'countries.txt')
        cr = load_vocab(r'content_ratings.txt')
        ge = load_vocab(r'genres.txt')
        x.Country = str(x.iloc[0,2]).split(', ', 1)[0]
        x.Genre = str(x.iloc[0,1]).split(', ', 1)[0]
        x = x.replace(['United States', 'United Kingdom'], ['USA', 'UK'])  # replace some of the data if nessesary

        x.Director = [dn[str(word)] if str(word) in dn else 0 for word in x.Director]
        x.Country = [ct[str(word)] if str(word) in ct else 0 for word in x.Country]
        x.Rated = [cr[str(word)] if str(word) in cr else 0 for word in x.Rated]
        x.Genre = [ge[str(word)] if str(word) in ge else 0 for word in x.Genre]  # encode the data
        x = x.rename(columns={'Director':'director_name', 'Genre':'genres',
                              'Country':'country', 'Rated':'content_rating'})  # rename the columns
        with open('Linearegression.pkl', 'rb') as f:  # load the model
            model = pickle.load(f)
        return model.predict(x)  # predict the box office

def pdf_create(name, box_office, ls, df, save_path):  # create the pdf document
    sg.one_line_progress_meter_cancel()
    name_encoded = sentence_encoder(name)
    plt.subplot(1,2,1)
    plot_review_time_increasing(ls, 'Evolution of Opinion in the Last Two Years', 'Change by Time', 'Score')
    plt.subplot(1,2,2)
    bar_review_scores_increasing(ls, 'Proportion of Scores', 'Score', 'Number of People')
    plt.savefig('graph.png')  # ploting the graph and save it
    content = list()  # start creating the pdf document using SimpleDocTemplate
    ls_modified = [1.05 if i>1 else i for i in ls]  # change the score that went beyond 1 to 1.05
    ls_modified = [0.05 if i<0.1 else i for i in ls_modified]  # change the score that went below 0.1 to 0.05
    content.append(Graphs.draw_title(name))  # format the document using the class created in py file 'pdf_maker'
    content.append(Graphs.draw_title(' '))
    content.append(Graphs.draw_title(' '))
    try:   # check if we can get the avg score and the estimated box office
        content.append(Graphs.draw_little_title('Score Estimated by LSTM Model'))
        content.append(Graphs.draw_img('graph.png'))
        content.append(Graphs.draw_little_title('Overall Score:'))
        content.append(Graphs.draw_title(str(sum(ls_modified)*10/len(ls_modified))))
        content.append(Graphs.draw_title(' '))
        content.append(Graphs.draw_title(' '))
        content.append(Graphs.draw_little_title('Most Positive Review:'))
        max_index = np.argmax(np.array(ls))
        content.append(Graphs.draw_text(df[max_index]))
        content.append(Graphs.draw_little_title('Least Positive Review:'))
        min_index = np.argmin(np.array(ls))
        content.append(Graphs.draw_text(df[min_index]))
        content.append(Graphs.draw_title(' '))
        content.append(Graphs.draw_title(' '))
        content.append(Graphs.draw_little_title('(Pre-alpha) Estimated Box Office:'))
        content.append(Graphs.draw_title(str(str(int(float(box_office)//1000000))+' million $')))  # round the number to the nearlest million
        outfilename = f"{name_encoded} analysis.pdf"
        outfilepath = os.path.join(save_path, outfilename)
        doc = SimpleDocTemplate(outfilepath, pagesize=A4)
        doc.build(content,onFirstPage=myLaterPages, onLaterPages=myLaterPages)
    except:  # if we could get neither esitimated score nor estimated box office, return this error file
        content = []
        content.append(Graphs.draw_title(name))
        content.append(Graphs.draw_title(' '))
        content.append(Graphs.draw_title(' '))
        content.append(Graphs.draw_title('Movie Review Not Found'))
        outfilename = f"{name_encoded} analysis.pdf"
        outfilepath = os.path.join(save_path, outfilename)
        doc = SimpleDocTemplate(outfilepath, pagesize=A4)
        doc.build(content, onFirstPage=Review_Error, onLaterPages=Review_Error)

    # deletethe json files we created
    file_to_delete = 'reviews.json'
    if os.path.exists(file_to_delete):
        os.remove(file_to_delete)
    file_to_delete = 'translated_file.json'
    if os.path.exists(file_to_delete):
        os.remove(file_to_delete)
    file_to_delete = 'reviews2.json'
    if os.path.exists(file_to_delete):
        os.remove(file_to_delete)
    file_to_delete = 'output.txt'
    if os.path.exists(file_to_delete):
        os.remove(file_to_delete)
    os.remove('graph.png')
