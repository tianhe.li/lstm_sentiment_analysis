import numpy as np
import json
import time
from googletrans import Translator
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import requests
import base64
import re
import pandas as pd
import os
import PySimpleGUI as sg
from deep_translator import GoogleTranslator
#I'm using it because I'm been blocked by google translator :(
counter = 0

#looking for the movie on allocine
def get_movie_from_name(movie_name:str):
    url = f"https://www.allocine.fr/rechercher/?q={movie_name}"
    response = requests.get(url)
    soup = BeautifulSoup(response.content, "html.parser")

    url_list = []
# Find all <h2> elements with class "meta-title" in the HTML content (soup)
    for title in soup.find_all("h2", class_ = "meta-title"):
        span = title.findChild("span")
        url_list.append(span)
# If no movie is found, raise a ValueError
    if url_list == None :
        raise ValueError(f"Aucun film n'a été trouvé avec {movie_name} comme query.")

# Call the movie_selector function with the list of URLs and the specified number of choices
    return url_list

# Extract the encoded URL from the movie's class attribute
def get_id_from_movie(movie):
    encoded_url = movie.get("class")[0]
    decoded_url = decode_href(encoded_url)
    id = get_num_from_string(decoded_url)
    return id

# Extract the first sequence of digits from the input string
def get_num_from_string(str:str) :
    result = re.search(r'\d+', str)
    if result:
        return int(result.group())
    raise ValueError("Pas d'ID dans l'url...")

# Split the encoded URL to remove specific characters and join the remaining parts
def decode_href(encoded_url:str):
    prep = encoded_url.split("ACr")
    data = "".join(prep)
# Decode the base64-encoded data and convert it to a string
    result = base64.b64decode(data).decode()
    return result


# Check if the URL is valid by parsing it and verifying the presence of scheme and netloc
def is_valid_url(url:str):
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False

# Construct the URL for the movie's review page with the given ID and page number
def get_review_page_from_id(id:int, page_number=1) :
    url = f"https://www.allocine.fr/film/fichefilm-{id}/critiques/spectateurs/recentes/?page={page_number}"

    if not is_valid_url(url) :
        raise ValueError("L'url obtenu n'est pas valide !")

    response = requests.get(url)
    return BeautifulSoup(response.content, "html.parser")

#scrap for comments
def get_all_reviews(soup:BeautifulSoup):
    reviews = []
    for review in soup.find_all('div', "content-txt review-card-content"):
        reviews.append(review.text)

    return reviews

# limit to 30, after this the key changes, and we have enough data
def get_number_of_page(soup:BeautifulSoup):
    pages = []
    for page in soup.find_all('a', class_="button button-md item"):
        pages.append(page.text)
    return int(pages[-1])

# get the result in a ditionnary
def get_reviews(url_list, index):
    movie = url_list[index]
    if movie == None :
        raise ValueError("no movie found")
    id = get_id_from_movie(movie)
    first_page = get_review_page_from_id(id)
    data_dict = {
        "title": first_page.title.string if first_page.title else None,
        "page1": get_all_reviews(first_page)
    }
    #print(get_number_of_page(first_page) + 1)
    for i in range(10):
        page = get_review_page_from_id(id, i)
        data_dict[f"page{i}"] = get_all_reviews(page)

    return data_dict

# get dictionnary info in a json file which can then be used for translation
def write_reviews(reviews_dict:dict):
    json_data = json.dumps(reviews_dict, ensure_ascii=False, indent=4)
    with open("reviews.json", "w", encoding="utf-8") as file:
        file.write(json_data)

# Function to recursively convert JSON values to strings
def convert_to_str(obj):
    if isinstance(obj, dict):
        return {key: convert_to_str(value) for key, value in obj.items()}
    elif isinstance(obj, list):
        return [convert_to_str(item) for item in obj]
    else:
        return str(obj)
    
#translator = Translator()
translator = GoogleTranslator(source='auto', target='en')
#I'm using it because I'm been blocked by google translator :(
# Function to recursively translate nested dictionaries or lists
def translate(data):
    global counter
    counter+=1
    
    sg.one_line_progress_meter(
        'loading', counter, 162,
        'Translating Data'
        )
    
    if isinstance(data, list):
        return [translate(item) for item in data]
    elif isinstance(data, dict):
        return {key: translate(value) for key, value in data.items()}
    elif isinstance(data, str):
        try:
# Translate the string from French to English
            #translation = translator.translate(data, src='fr', dest='en')
            translation = translator.translate(data)
            #I'm using it because I'm been blocked by google translator :(
# Add a delay to avoid Google Translate API limits
            time.sleep(0.01)
            return translation
        except:
            return data
    else:
        return data

def get_data_step_1(FILM_NAME):
    file_to_delete = 'reviews.json'
    if os.path.exists(file_to_delete):
        os.remove(file_to_delete)
    file_to_delete = 'translated_file.json'
    if os.path.exists(file_to_delete):
        os.remove(file_to_delete)
    file_to_delete = 'reviews2.json'
    if os.path.exists(file_to_delete):
        os.remove(file_to_delete)
    file_to_delete = 'output.txt'
    if os.path.exists(file_to_delete):
        os.remove(file_to_delete)
    url_list = get_movie_from_name(FILM_NAME)
    CHOICES = min([10, len(url_list)])
    url_list = url_list[0:CHOICES]
    movie_list = []
    for i in range(CHOICES):
        movie_list.append(f"{url_list[i].text}")
    
    return url_list, movie_list

def get_data_step_2(url_list, index):
    global counter
    counter = 0
    
    reviews = get_reviews(url_list, index)
    write_reviews(reviews)
    with open('reviews.json', 'r', encoding='utf-8') as json_file:
        data = json.load(json_file)
    input_file_path = "reviews.json"
    output_file_path = "reviews2.json"

    try:
        # Read the JSON data from the input file
        with open(input_file_path, 'r', encoding='utf-8') as input_file:
            json_data = json.load(input_file)

        # Convert the JSON data to contain only string values
        json_data_str = convert_to_str(json_data)

        # Write the modified JSON data to the output file
        with open(output_file_path, 'w') as output_file:
            json.dump(json_data_str, output_file, indent=2)

    except FileNotFoundError:
        print(f"Input file '{input_file_path}' not found.")
    except json.JSONDecodeError:
        print(f"Error decoding JSON in '{input_file_path}'. Make sure it's a valid JSON file.")

    # Define your input and output file names
    input_file = 'reviews2.json'
    output_file = 'translated_file.json'
    counter=0
    with open(input_file, 'r', encoding='utf-8') as input_f, open(output_file, 'w', encoding='utf-8') as output_f:
        data = json.load(input_f)
        translated_data = translate(data)
        json.dump(translated_data, output_f, ensure_ascii=False, indent=4)
    sg.one_line_progress_meter_cancel()

def get_movie_details(api_key, url_list, index):
    movie = url_list[index]
    movie = translate(f'{movie.text}')
    base_url = f'http://www.omdbapi.com/?apikey={api_key}&t={movie}'
    # Make a request to the OMDB API
    response = requests.get(base_url)
    try:
        data = pd.DataFrame(response.json()).head(1)
        # Extract relevant information
        data = data[['Director', 'Genre', 'Country', 'Rated']]
        return data
    except:
        print('movie not found')
        arr=np.zeros((1,4))
        return pd.DataFrame(arr, columns=['Director', 'Genre', 'Country', 'Rated'])




