from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import Table, SimpleDocTemplate, Paragraph, Image  
from reportlab.lib.pagesizes import letter  
from reportlab.lib.styles import getSampleStyleSheet  
from reportlab.lib import colors  
from reportlab.graphics.charts.barcharts import VerticalBarChart  
from reportlab.graphics.charts.legends import Legend  
from reportlab.graphics.shapes import Drawing  
from reportlab.lib.units import cm

pdfmetrics.registerFont(TTFont('Times New Roman', 'Times New Roman.ttf'))  # register the font
font = 'Times New Roman'

class Graphs:
    # draw title
    @staticmethod
    def draw_title(title: str):
        # get style sheet
        style = getSampleStyleSheet()
        # get title style
        ct = style['Heading1']
        # set settings
        ct.fontName = font     
        ct.fontSize = 30
        ct.leading = 30    
        ct.textColor = colors.white
        ct.alignment = 1
        ct.bold = True
        # center it
        return Paragraph(title, ct)
      
  # draw title, smaller
    @staticmethod
    def draw_little_title(title: str):
        # get style sheet
        style = getSampleStyleSheet()
        # get title style
        ct = style['Normal']
        # set settings
        ct.fontName = font
        ct.fontSize = 20
        ct.leading = 30
        ct.textColor = colors.white
        # center it
        return Paragraph(title, ct)

    # draw content
    @staticmethod
    def draw_text(text: str):
        # get style sheet
        style = getSampleStyleSheet()
        # get content style
        ct = style['Normal']
        ct.fontName = font
        ct.fontSize = 12
        ct.wordWrap = 'CJK'     # switch lines automaticly
        ct.alignment = 0
        ct.firstLineIndent = 32
        ct.leading = 25
        ct.textColor = colors.white
        return Paragraph(text, ct)

    # draw picture
    @staticmethod
    def draw_img(path):
        img = Image(path)       # read path
        img.drawWidth = 16*cm        # width
        img.drawHeight = 12*cm       # height
        return img
