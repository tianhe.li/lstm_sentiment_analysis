print('loading...')
# load the modules
import PySimpleGUI as sg
import LSTM_modules as lstmm
import benji_modules as benm
import matplotlib.pyplot as plt
import os
# basic informations
api_key = '6c0c9ae4'
my_version = 'LSTM v2.4'

print('done')
if __name__ == '__main__':
    sg.theme('DarkBlue2')  # choosing the theme
    layout =[   [sg.Text('Film Analysis')],  # making the layout of the default page
                 [sg.Text('Enter a movie name'), sg.InputText()],

                [sg.Button('check'), sg.Button('exit')]]
                    
    window = sg.Window(my_version, layout)
    while True:
        event, value = window.read()
        if event in (None, 'check') and value[0] != '':  # process the movie name when it's been enetered while 'check' is been clicked
            txt = value[0]  # get the text
            window.close()
            url_list, movie_list = benm.get_data_step_1(txt)  # get the list of url and the list of movie names
            layout = [   [sg.Text('Which one is the movie you expect?')],
                [sg.Combo(movie_list,
                         default_value='choose a movie',
                         size=(50,1))],
                [sg.Button('SEARCH')]]
            window = sg.Window(my_version, layout)  # ask users to choose the specific movie
        else:  # travel back to the first page while telling the user the situation when something went wrong
            window.close()
            layout =[   [sg.Text('Film Analysis')],
                         [sg.Text('oops something went wrong')],
                        [sg.Text('Enter your film name again'), sg.InputText()],
                        [sg.Button('check'), sg.Button('exit')]]
            window = sg.Window(my_version, layout)
        
        if event in (None, 'exit'):  # exit the software when user click exit
            break
        
        if event in (None, 'SEARCH') and value[0] in movie_list:
            index = movie_list.index(value[0])
            df_detail = benm.get_movie_details(api_key, url_list, index)  # get movie details
            benm.get_data_step_2(url_list, index)  # get reviews, translate them, and save as json files
            df = lstmm.load_json_file(r'translated_file.json')  # read the reviews
            try:
                ls = lstmm.df_quick_predict(df)  # do the sentiment analysis using lstm model
            except:
                ls = [0]  # in case there was enough data to do the sentiment analysis, return 0
            box_office = lstmm.box_office_predict(lstmm.box_office_preprocess(df_detail, ls))  # do the box office prediction
            save_path = sg.popup_get_folder('Where do you want to save the result document?')  # ask the user for the path saving the file
            if save_path == '':  # if the user did not enter the path, ask them to enter the path again
                while save_path == '':
                    sg.popup('oops, path is empty')
                    save_path = sg.popup_get_folder('Where do you want to save the result document?')
            try:  # create and save the pdf document
                name_encoded = lstmm.sentence_encoder(value[0])
                outfilename = f"{name_encoded} analysis.pdf"
                outfilepath = os.path.join(save_path, outfilename)
                output = str('file saved at:  ' + outfilepath)
                sg.popup(output, title='done')  # tell the user where the document was saved
                lstmm.pdf_create(value[0], box_office, ls, df, save_path)
                break
            except:
                print("Invalid Path or Progress's Been Cancelled")  # if the user cancelled the process or entered an invalid path, print the situation 
                break
        elif event in (None, 'SEARCH') and value[0] not in movie_list:
            sg.popup_ok('oops, choose a movie name before click on "SEARCH"')  # in case user clicked on search with out choosing movie, popup this page
